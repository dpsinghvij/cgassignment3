package cg.assignment.third.utils;

/**
 * Created by davinder on 17/10/16.
 */
public class Constants {
    public static final int SIZE = 128;
    public static final int NO_OF_COL = 512;
    public static final int NO_OF_ROWS = 512;
    public static final float XMIN = -0.0175f;
    public static final float XMAX = 0.0175f;
    public static final float YMIN = -0.0175f;
    public static final float YMAX = 0.0175f;
    public static final float FOCAL_LENGTH = .05f;
    public static final float INTENSITY_OF_POINT_SOURCE = 255;
    public static final float KD = .75f;
    public static final int THRESHOLD_OPACITY = 40;
    public static final float SAMPLING_INTERVAL = 1;
    public static final String help="Movement Control\n" +
            "Move camera using left,right,top,bottom ARROW keys\n" +
            "or\n" +
            "Dragging mouse around the screen\n" +
            "Zoom In \t Q\n" +
            "Zoom Out\t W\n";
    /*public static float VRP[] = new float[]{192.0f, 160.0f, 500f};
    public static float VPN[] = new float[]{-128.0f, -96.0f, -436.0f};
    public static float VUP[] = new float[]{0.0f, 1.0f, 0.0f};*/
    /*public static float VRP[] = new float[]{128.0f, 64.0f, 250.0f};
    public static float VPN[] = new float[]{-64.0f, 0.0f, -186.0f};
    public static float VUP[] = new float[]{0.0f, 1.0f, 0.0f};*/
    /*
    Top Angle
    public static float VRP[] = new float[]{64.0f, 500.0f, 250.0f};
    public static float VPN[] = new float[]{0f, -436f, -186.0f};
    public static float VUP[] = new float[]{0.0f, 1.0f, 0.0f};*/

    // Bottom Angle
    public static float VRP[] = new float[]{128.0f, 64.0f, 250.0f};
    public static float VPN[] = new float[]{-64.0f, 0.0f, -186.0f};
    public static float VUP[] = new float[]{0.0f, 1.0f, 0.0f};
    public static float Light[] = new float[]{0.577f, -0.577f, -0.577f};
    /*public static String sphereJson= "{\n" +
            "  \"centerOfSphere\":{ \"x\":1,\"y\":1,\"z\":1},\n" +
            "  \"radius\": 1,\n" +
            "  \"kd\":0.75\n" +
            "}";
    public static String polygonJson="{\n" +
            "  \"vertices\":[{ \"x\":0,\"y\":0,\"z\":0},\n" +
            "    {  \"x\":0,\"y\":0,\"z\":2},\n" +
            "    { \"x\":2,\"y\":0,\"z\":2},\n" +
            "    {  \"x\":2,\"y\":0,\"z\":0}],\n" +
            "  \"normalVector\":{\"x\":0,\"y\":1,\"z\":0},\n" +
            "  \"diffusionCoefficient\":0.8\n" +
            "}";
    public static String cameraJson= "{\n" +
            "  \"pointVrp\":{ \"x\":1,\"y\":2,\"z\":3.5},\n" +
            "  \"vectorVpn\":{ \"x\":0,\"y\":-1,\"z\":-2.5},\n" +
            "  \"vectorVup\":{ \"x\":0,\"y\":1,\"z\":0}\n" +
            "}";*/
}
